﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Console.Beep(10000, 100);
            }
            else if (e.Button == MouseButtons.Right)
            {
                Console.Beep(5000, 100);
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Text = $"X:{e.X} Y:{e.Y}";
        }

        private void clickMeButton_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Test", "Message box title", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
            if (result == DialogResult.Yes)
            {
                MessageBox.Show("YES");
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("NO");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            if (adminCheckBox.Checked)
            {
                MessageBox.Show(nameTextBox.Text);
            }
        }

        private void progressNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            var value = progressNumericUpDown.Value;
            statusProgressBar.Value = (int)value;
        }
    }
}
