﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class AddForm : Form
    {
        enum Mode { Add, Edit };

        private Person personResult;
        private string uploadedPhoto;
        private Mode mode;
        

        public AddForm()
        {
            InitializeComponent();

            mode = Mode.Add;
            var currentDirectory = Directory.GetCurrentDirectory();
            uploadedPhoto = $@"{currentDirectory}\placeholder.jpg";
            photoPictureBox.Image = Image.FromFile(uploadedPhoto);
        }

        public AddForm(Person person)
        {
            InitializeComponent();

            mode = Mode.Edit;
            Text = "Edit contact";

            nameTextBox.Text = person.Name;
            surnameTextBox.Text = person.Surname;
            phoneMaskedTextBox.Text = person.Phone;
            emailTextBox.Text = person.Email;
            birthDatePicker.Value = person.BirthDate;
            favoriteCheckBox.Checked = person.Favorite;

            switch (person.Gender)
            {
                case Gender.Male:
                    maleRadioButton.Checked = true;
                    break;
                case Gender.Female:
                    femaleRadioButton.Checked = true;
                    break;
                case Gender.Other:
                    otherRadioButton.Checked = true;
                    break;
            }

            uploadedPhoto = person.Photo;
            photoPictureBox.Image = Image.FromFile(person.Photo);
        }

        public new Person ShowDialog()
        {
            base.ShowDialog();
            return personResult;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            var person = new Person();
            
            person.Name = nameTextBox.Text;
            person.Surname = surnameTextBox.Text;
            person.Phone = phoneMaskedTextBox.Text;
            person.Email = emailTextBox.Text;
            person.BirthDate = birthDatePicker.Value;
            person.Favorite = favoriteCheckBox.Checked;

            if (maleRadioButton.Checked)
                person.Gender = Gender.Male;
            if (femaleRadioButton.Checked)
                person.Gender = Gender.Female;
            if (otherRadioButton.Checked)
                person.Gender = Gender.Other;

            person.Photo = uploadedPhoto;

            personResult = person;

            Close();
        }

        private void uploadPhotoButton_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Image Files (*.jpg,*.png)|*.jpg;*.png";
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var path = SavePhoto(dialog.FileName);
                uploadedPhoto = path;
                photoPictureBox.Image = Image.FromFile(path);
            }
        }

        private string SavePhoto(string path) 
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                if (!Directory.Exists($@"{currentDirectory}\Photos"))
                {
                    Directory.CreateDirectory($@"{currentDirectory}\Photos");
                }

                var extension = Path.GetExtension(path);
                var newPath = $@"{currentDirectory}\Photos\{Guid.NewGuid()}{extension}";

                File.Copy(path, newPath);

                return newPath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return $@"{currentDirectory}\placeholder.jpg";
            }
        }
    }
}
