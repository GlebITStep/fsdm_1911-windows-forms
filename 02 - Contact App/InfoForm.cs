﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class InfoForm : Form
    {
        public InfoForm(Person person)
        {
            InitializeComponent();

            fullNameLabel.Text = $"{person.Name} {person.Surname}";
            phoneTextBox.Text = person.Phone;
            emailTextBox.Text = person.Email;
            genderLabel.Text = person.Gender.ToString();
            dateLabel.Text = person.BirthDate.ToShortDateString();
            if (!person.Favorite)
                favoriteLabel.Visible = false;
            imagePictureBox.Image = Image.FromFile(person.Photo);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
