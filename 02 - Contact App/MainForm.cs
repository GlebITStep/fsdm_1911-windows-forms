﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class MainForm : Form
    {
        public List<Person> People { get; set; } = new List<Person>();

        public MainForm()
        {
            InitializeComponent();

            LoadContacts();
            peopleListBox.Items.AddRange(People.ToArray());
        }

        private void SaveContacts() 
        {
            var json = JsonConvert.SerializeObject(People);
            File.WriteAllText("contacts.json", json);
        }

        private void LoadContacts() 
        {
            var json = File.ReadAllText("contacts.json");
            People = JsonConvert.DeserializeObject<List<Person>>(json);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            SaveContacts();
            base.OnClosing(e);
        }

        private void peopleListBox_DoubleClick(object sender, EventArgs e)
        {
            var selectedPerson = peopleListBox.SelectedItem as Person;
            if (selectedPerson != null)
            {
                var infoForm = new InfoForm(selectedPerson);
                infoForm.ShowDialog();
            }
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            var selectedPerson = peopleListBox.SelectedItem as Person;
            if (selectedPerson == null)
            {
                MessageBox.Show("Contact is not selected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var infoForm = new InfoForm(selectedPerson);
                infoForm.ShowDialog();

                //MessageBox.Show(selectedPerson.FullInfo(), "Contact Info", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var addForm = new AddForm();
            var personResult = addForm.ShowDialog();

            if (personResult != null)
            {
                People.Add(personResult);
                peopleListBox.Items.Add(personResult);
            }

            //peopleListBox.Items.Clear();
            //peopleListBox.Items.AddRange(People.ToArray());
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            int index = peopleListBox.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("Contact is not selected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else 
            {
                People.RemoveAt(index);
                peopleListBox.Items.RemoveAt(index);
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            var selectedPerson = peopleListBox.SelectedItem as Person;
            if (selectedPerson == null)
            {
                MessageBox.Show("Contact is not selected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else 
            {
                var addForm = new AddForm(selectedPerson);
                var personResult = addForm.ShowDialog();

                if (personResult != null)
                {
                    var index = peopleListBox.SelectedIndex;
                    
                    People.RemoveAt(index);
                    peopleListBox.Items.RemoveAt(index);

                    People.Insert(index, personResult);
                    peopleListBox.Items.Insert(index, personResult);
                }
            }
        }
    }
}
