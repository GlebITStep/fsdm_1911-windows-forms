﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Gender Gender { get; set; }
        public bool Favorite { get; set; }
        public string Photo { get; set; }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }

        public string FullInfo()
        {
            return $"{Name} {Surname}\n{Phone}\n{Email}";
        }
    }
}
