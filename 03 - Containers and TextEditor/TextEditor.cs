﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class TextEditor : Form
    {
        public TextEditor()
        {
            InitializeComponent();

            fontSizeToolComboBox.SelectedIndex = 0;

            fontToolButton.Text = richTextBox.Font.Name;
            fontToolButton.Font = richTextBox.Font;

            Bitmap bmp = new Bitmap(100, 100);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.Black);

            colorToolButton.Image = bmp;
        }

        private void colorToolButton_Click(object sender, EventArgs e)
        {
            var dialog = new ColorDialog();
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                richTextBox.SelectionColor = dialog.Color;

                Bitmap bmp = new Bitmap(100, 100);
                Graphics g = Graphics.FromImage(bmp);
                g.Clear(dialog.Color);

                colorToolButton.Image = bmp;
            }
        }

        private void fontToolButton_Click(object sender, EventArgs e)
        {
            var dialog = new FontDialog();
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var fontSize = float.Parse(fontSizeToolComboBox.SelectedItem.ToString());
                var font = new Font(dialog.Font.FontFamily, fontSize);

                richTextBox.SelectionFont = font;
                fontToolButton.Text = font.Name;
                fontToolButton.Font = new Font(dialog.Font.FontFamily, 8);
            }
        }

        private void fontSizeToolComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var fontSize = float.Parse(fontSizeToolComboBox.SelectedItem.ToString());
            var font = new Font(richTextBox.Font.FontFamily, fontSize);
            richTextBox.SelectionFont = font;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var text = richTextBox.Text;

            var dialog = new SaveFileDialog();
            dialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                File.WriteAllText(dialog.FileName, text);
            }
        }

        private void rtfFlieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var text = richTextBox.Rtf;

            var dialog = new SaveFileDialog();
            dialog.Filter = "RTF files (*.rtf)|*.rtf|All files (*.*)|*.*";
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                File.WriteAllText(dialog.FileName, text);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Text files (*.rtf,*.txt)|*.rtf;*.txt|All files (*.*)|*.*";
            var resutlt = dialog.ShowDialog();

            if (resutlt == DialogResult.OK)
            {
                var text = File.ReadAllText(dialog.FileName);

                if (Path.GetExtension(dialog.FileName) == ".txt")
                    richTextBox.Text = text;
                else if (Path.GetExtension(dialog.FileName) == ".rtf")
                    richTextBox.Rtf = text;
                else
                    richTextBox.Text = text;
                //MessageBox.Show("Invalid file format!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void richTextBox_TextChanged(object sender, EventArgs e)
        {
            textLengthToolStripStatusLabel.Text = $"Text length: {richTextBox.Text.Length}";
        }
    }
}
