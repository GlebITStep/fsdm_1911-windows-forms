﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsPaint
{
    public partial class ImageForm : Form
    {
        private PictureBox picture;

        public Point OldLocation { get; set; }
        public GraphicsState State { get; set; }
        Graphics Graphics { get; set; }
        public PictureBox Picture { get => pictureBox1; }

        public ImageForm()
        {
            InitializeComponent();
            Graphics = pictureBox1.CreateGraphics();
        }

        private void MouseDown(object sender, MouseEventArgs e)
        {
            OldLocation = e.Location;
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            var color = (ParentForm as PaintForm).Color;
            var size = (ParentForm as PaintForm).Size;

            if (e.Button == MouseButtons.Left)
            {
                Pen pen = new Pen(color, size);
                Graphics.DrawLine(pen, OldLocation, e.Location);
                OldLocation = e.Location;

                State = Graphics.Save();
            }
        }
    }
}
