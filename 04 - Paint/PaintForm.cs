﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsPaint
{
    public partial class PaintForm : Form
    {
        public Color Color { get; set; } = Color.Black;
        public int Size { get; set; } = 5;

        public PaintForm()
        {
            InitializeComponent();

            DrawColorImage();


            toolStripComboBox1.Items.AddRange(Enumerable.Range(1, 20).Select(x => x as object).ToArray());
            toolStripComboBox1.SelectedIndex = 4;

            Timer timer = new Timer();
            timer.Interval = 100;
            timer.Tick += Timer_Tick;
            //timer.Start();
        }

        private void DrawColorImage() 
        {
            var image = new Bitmap(100, 100);
            Graphics g = Graphics.FromImage(image);
            g.Clear(Color);
            toolStripButton1.Image = image;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Random rand = new Random();
            var r = rand.Next(0, 255);
            var g = rand.Next(0, 255);
            var b = rand.Next(0, 255);

            Graphics graphics = CreateGraphics();
            Brush brush = new SolidBrush(Color.FromArgb(r, g, b));
            //graphics.FillRectangle(brush, 10, 10, 100, 100);
            graphics.FillRectangle(brush, 0, 0, Width, Height);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var imageFrom = new ImageForm();
            imageFrom.MdiParent = this;
            imageFrom.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var dialog = new ColorDialog();
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                Color = dialog.Color;
                DrawColorImage();
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Size = (int)toolStripComboBox1.SelectedItem;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                var child = ActiveMdiChild as ImageForm;
                child.Picture.Image.Save(dialog.FileName, ImageFormat.Png);


                //var filename = dialog.FileName;
                //var graphics = ActiveMdiChild.CreateGraphics();
                //var width = ActiveMdiChild.Width;
                //var height = ActiveMdiChild.Height;
                //var image = new Bitmap(width, height, graphics);
                //image.Save(filename, ImageFormat.Png);
            }
        }
    }
}
