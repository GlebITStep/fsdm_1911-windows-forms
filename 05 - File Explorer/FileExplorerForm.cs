﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace WindowsFormsExplorer
{
    public partial class FileExplorerForm : Form
    {
        public FileExplorerForm()
        {
            InitializeComponent();

            IEnumerable<string> rootDirectories = Directory.EnumerateDirectories(@"C:\");
            foreach (string directoryPath in rootDirectories)
            {
                TreeNode treeNode = new TreeNode(Path.GetFileName(directoryPath));
                treeNode.Tag = directoryPath;
                foldersTreeView.Nodes.Add(treeNode);

                try
                {
                    var childDirectories = Directory.EnumerateDirectories(directoryPath);
                    foreach (string path in childDirectories)
                    {
                        TreeNode childTreeNode = new TreeNode(Path.GetFileName(path));
                        childTreeNode.Tag = path;
                        treeNode.Nodes.Add(childTreeNode);
                    }
                }
                catch (Exception) { }

            }

        }

        private void foldersTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode item in e.Node.Nodes)
            {
                if (item.Nodes.Count == 0)
                {
                    try
                    {
                        var directories = Directory.EnumerateDirectories(item.Tag as string);
                        foreach (var path in directories)
                        {
                            TreeNode treeNode = new TreeNode(Path.GetFileName(path));
                            treeNode.Tag = path;
                            item.Nodes.Add(treeNode);
                        }
                    }
                    catch (Exception) { }
                }
            }
        }

        private void foldersTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var path = e.Node.Tag as string;
            OpenFolder(path);
        }

        private void largeIconsToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            filesListView.View = View.LargeIcon;
        }

        private void smallIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filesListView.View = View.SmallIcon;
        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filesListView.View = View.List;
        }

        private void tileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filesListView.View = View.Tile;
        }

        private void filesListView_ItemActivate(object sender, EventArgs e)
        {
            var selection = filesListView.SelectedItems[0];
            var path = selection.Tag as string;
            if (Path.HasExtension(path))
            {
                Process.Start(path);
            }
            else
            {
                OpenFolder(path);
            }
        }

        private void OpenFolder(string path) 
        {
            pathTextBox.Text = path;

            try
            {
                var filesCount = Directory.EnumerateFiles(path).Count();
                filesToolStripStatusLabel.Text = $"Files: {filesCount}";


                var entries = Directory.EnumerateFileSystemEntries(path);

                filesListView.Clear();
                foreach (var entry in entries)
                {
                    ListViewItem item;

                    if (Path.HasExtension(entry))
                        item = new ListViewItem(Path.GetFileName(entry), 1);
                    else
                        item = new ListViewItem(Path.GetFileName(entry), 0);

                    //ListViewItem item = new ListViewItem(Path.GetFileName(entry), Path.HasExtension(entry) ? 1 : 0);

                    item.Tag = entry;
                    filesListView.Items.Add(item);
                }
            }
            catch (Exception) { }

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            var lastSymbolIndex = pathTextBox.Text.LastIndexOf(@"\");
            var path = pathTextBox.Text.Substring(0, lastSymbolIndex);
            OpenFolder(path);
        }
    }
}
