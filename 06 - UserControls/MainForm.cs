﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsUserControls
{
    public partial class MainForm : Form
    {
        public UserControl1 UserControl1 { get; set; }
        public UserControl2 UserControl2 { get; set; }

        public MainForm()
        {
            InitializeComponent();

            UserControl1 = new UserControl1();
            UserControl2 = new UserControl2();

            UserControl1.Dock = DockStyle.Fill;
            UserControl2.Dock = DockStyle.Fill;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            panel1.Controls.Add(UserControl1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            panel1.Controls.Add(UserControl2);
        }
    }
}
