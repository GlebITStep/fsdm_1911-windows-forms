﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;
using ToDoListMVP.Views;

namespace ToDoListMVP.Presenters
{
    public class ToDoListPresenter
    {
        public ToDoListView View { get; set; }

        public List<ToDo> Tasks { get; set; }

        public ToDoListPresenter(ToDoListView view)
        {
            View = view;

            Tasks = new List<ToDo>
            {
                new ToDo 
                { 
                    Id = Guid.NewGuid().ToString(), 
                    Title = "Do my homework", 
                    Description = "Translator App Windows Forms"
                },
                new ToDo
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "Feed my cat",
                    Description = "At 13:00"
                },
                new ToDo
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "Do my homework",
                    Description = "Browser App Windows Forms"
                }
            };

            View.UpdateTasks(Tasks);
        }

        public void RemoveTask(string id)
        {
            var index = Tasks.FindIndex(x => x.Id == id);
            Tasks.RemoveAt(index);
            View.UpdateTasks(Tasks);
        }

        public void AddTask()
        {
            var task = new ToDo
            {
                Id = Guid.NewGuid().ToString(),
                Title = "Test",
                Description = "Test"
            };
            Tasks.Add(task);
            View.UpdateTasks(Tasks);
        }
    }
}
