﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoListMVP.Presenters;
using ToDoListMVP.Views;

namespace ToDoListMVP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var toDoListView = new ToDoListView();
            var toDoListPresenter = new ToDoListPresenter(toDoListView);

            toDoListView.Presenter = toDoListPresenter;

            Application.Run(toDoListView);
        }
    }
}
