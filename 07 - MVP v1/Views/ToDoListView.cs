﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoListMVP.Models;
using ToDoListMVP.Presenters;

namespace ToDoListMVP.Views
{
    public partial class ToDoListView : Form
    {
        public ToDoListPresenter Presenter { get; set; }

        public ToDoListView()
        {
            InitializeComponent();
        }

        public void UpdateTasks(List<ToDo> toDos)
        {
            tasksListBox.Items.Clear();
            tasksListBox.Items.AddRange(toDos.ToArray());
            descriptionTextBox.Text = "";
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (tasksListBox.SelectedItem != null)
            {
                var id = (tasksListBox.SelectedItem as ToDo).Id;
                Presenter.RemoveTask(id);
            }
        }

        private void tasksListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tasksListBox.SelectedItem != null)
            {
                var description = (tasksListBox.SelectedItem as ToDo).Description;
                descriptionTextBox.Text = description;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Presenter.AddTask();
        }
    }
}
