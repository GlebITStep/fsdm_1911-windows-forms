﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;
using ToDoListMVP.Services;
using ToDoListMVP.Views;

namespace ToDoListMVP.Presenters
{
    class ToDoAddPresenter
    {
        private readonly IToDoAddView view;
        private readonly IToDoRepository repository;

        public ToDoAddPresenter(IToDoAddView view, IToDoRepository repository)
        {
            this.view = view;
            this.repository = repository;

            view.Add += AddTask;
        }

        private void AddTask()
        {
            var task = new ToDo
            {
                Id = Guid.NewGuid().ToString(),
                Title = view.Title,
                Description = view.Description
            };
            repository.AddToDo(task);
        }
    }
}
