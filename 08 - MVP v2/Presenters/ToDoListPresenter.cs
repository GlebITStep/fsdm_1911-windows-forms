﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;
using ToDoListMVP.Services;
using ToDoListMVP.Views;

namespace ToDoListMVP.Presenters
{
    public class ToDoListPresenter
    {
        private IToDoListView view;
        private IToDoRepository repository;

        public ToDoListPresenter(IToDoListView view, IToDoRepository repository)
        {
            this.view = view;
            this.repository = repository;

            view.UpdateTasks(repository.GetToDos());
            view.Add += AddTask;
            view.Remove += RemoveTask;
        }

        public void RemoveTask(string id)
        {
            repository.RemoveToDo(id);
            view.UpdateTasks(repository.GetToDos());
        }

        public void AddTask()
        {
            Program.OpenAddView();
            view.UpdateTasks(repository.GetToDos());
        }
    }
}
