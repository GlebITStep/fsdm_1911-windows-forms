﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoListMVP.Presenters;
using ToDoListMVP.Services;
using ToDoListMVP.Views;

namespace ToDoListMVP
{
    static class Program
    {
        static IToDoRepository toDoRepository;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            toDoRepository = new JsonToDoRepository();

            var toDoListView = new ToDoListView();
            var toDoListPresenter = new ToDoListPresenter(toDoListView, toDoRepository);

            Application.Run(toDoListView);
        }

        public static void OpenAddView()
        {
            var toDoAddView = new ToDoAddView();
            var toDoAddPresenter = new ToDoAddPresenter(toDoAddView, toDoRepository);
            toDoAddView.ShowDialog();
        }
    }
}
