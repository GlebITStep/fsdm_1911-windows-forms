﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;

namespace ToDoListMVP.Services
{
    public interface IToDoRepository
    {
        IEnumerable<ToDo> GetToDos();
        void AddToDo(ToDo toDo);
        void RemoveToDo(string id);
    }
}
