﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;

namespace ToDoListMVP.Services
{
    class JsonToDoRepository : IToDoRepository
    {
        public IEnumerable<ToDo> GetToDos()
        {
            if (File.Exists("tasks.json"))
            {
                return Load();
            }
            else
            {
                File.Create("tasks.json").Close();
                return new List<ToDo>();
            }
        }

        public void AddToDo(ToDo toDo)
        {
            var tasks = Load();
            tasks.Add(toDo);
            Save(tasks);
        }

        public void RemoveToDo(string id)
        {
            var tasks = Load();
            tasks.RemoveAll(x => x.Id == id);
            Save(tasks);
        }

        private void Save(IEnumerable<ToDo> toDos)
        {
            var json = JsonConvert.SerializeObject(toDos);
            File.WriteAllText("tasks.json", json);
        }

        private List<ToDo> Load()
        {
            var json = File.ReadAllText("tasks.json");
            var tasks = JsonConvert.DeserializeObject<List<ToDo>>(json);
            if (tasks == null)
                tasks = new List<ToDo>();
            return tasks;
        }
    }
}
