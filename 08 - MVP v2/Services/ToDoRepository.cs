﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;

namespace ToDoListMVP.Services
{
    class ToDoRepository : IToDoRepository
    {
        private List<ToDo> tasks;

        public ToDoRepository()
        {
            tasks = new List<ToDo>
            {
                new ToDo
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "Do my homework",
                    Description = "Translator App Windows Forms"
                },
                new ToDo
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "Feed my cat",
                    Description = "At 13:00"
                },
                new ToDo
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "Do my homework",
                    Description = "Browser App Windows Forms"
                }
            };
        }

        public IEnumerable<ToDo> GetToDos()
        {
            return tasks;
        }

        public void AddToDo(ToDo toDo)
        {
            tasks.Add(toDo);
        }

        public void RemoveToDo(string id)
        {
            tasks.RemoveAll(x => x.Id == id);
        }
    }
}
