﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListMVP.Views
{
    public interface IToDoAddView
    {
        string Title { get; set; }
        string Description { get; set; }

        event Action Add;
    }
}
