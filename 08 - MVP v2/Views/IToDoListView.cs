﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListMVP.Models;

namespace ToDoListMVP.Views
{
    public interface IToDoListView
    {
        void UpdateTasks(IEnumerable<ToDo> toDos);

        event Action Add;

        event Action<string> Remove;
    }
}
