﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToDoListMVP.Models;

namespace ToDoListMVP.Views
{
    public partial class TestToDoListView : Form, IToDoListView
    {
        public TestToDoListView()
        {
            InitializeComponent();
        }

        public event Action Add;
        public event Action<string> Remove;

        public void UpdateTasks(IEnumerable<ToDo> toDos)
        {
            listView1.Clear();
            foreach (var item in toDos)
            {
                var listViewItem = new ListViewItem(item.Title, 0);
                listViewItem.Tag = item;
                listView1.Items.Add(listViewItem);
            }
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (listView1.SelectedItems.Count != 0)
                {
                    foreach (ListViewItem item in listView1.SelectedItems)
                    {
                        var id = (item.Tag as ToDo).Id;
                        Remove?.Invoke(id);
                    }
                }
            }
        }
    }
}
