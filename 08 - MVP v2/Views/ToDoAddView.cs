﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToDoListMVP.Views
{
    public partial class ToDoAddView : Form, IToDoAddView
    {
        public ToDoAddView()
        {
            InitializeComponent();
        }

        public string Title 
        { 
            get => titleTextBox.Text; 
            set => titleTextBox.Text = value; 
        }

        public string Description 
        { 
            get => descriptionTextBox.Text; 
            set => descriptionTextBox.Text = value; 
        }

        public event Action Add;

        private void addButton_Click(object sender, EventArgs e)
        {
            Add?.Invoke();
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
